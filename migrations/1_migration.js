const Migrations = artifacts.require('EskiLPToken')

module.exports = function(deployer) {
    deployer.deploy(
        Migrations,
        '0x92eAB14Dda192a8314FCC8cCBb3a283b64EB2760',
        '0x4abD3D3817a86D11416dD370e37aDE95a0FbdCCa'
    )
}
